Readme for SccTextX,  Version 2008-11-12


If you already used the first version of SccTextX,  you may got an error like 
     "Wrong SCCTEXTX version" 
when doing check-ins or Check-Outs. This is because all SCCTEXT versions write 
an identifier into the SCA/VCA/FRA texxtfiles. 
If you open those files with any texteditor, the very first line reads 
something like "SCCTEXT Version 4.0.0.2". This is the version string of the 
original MS-routine as well as the PL_SCCTEXT.PRG as well as this new version 
from 2008-11-12.  
Only in the first incarnation of SCCTEXTX I had that clever idea to change 
that string to advertise "SCCTEXTX version 1.0.0.1". Looking back this was 
a bad idea, since it renders all your old files useless ;) Thus in the new 
version I reverted that change back to the original string.   This new version 
additionally accepts now both version strings, thus you shouldn't get any 
compatibility problems anymore.

Why to use: 
Consistent and case insensitive sorting of methods, objects, and 
properties, resulting in much less comparison errors when doing a Diff. 
Bugfixes and speed improvements, several language localisations.

How to use: 
Just copy this prg into your HOME() folder and change the entry 
"Text Generation" under SysMenu/Tools/Options/Projects to point to that new file.



wOOdy  

"*��) 
�.���.�*��) �.�*�) 
(�.��. (�.�` * 
.�`.Visual FoxPro: It's magic ! 
(�.�``��* 



